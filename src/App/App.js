import '../index.html';
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';
import {LocalEvent} from './LocalEvent';
import {RefreshMap} from './RefreshMap';
import appConfig from '../../app.config';

const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

class App {

    domLocalEventName;
    domLocalEventContent;
    domLocalEventStart;
    domLocalEventEnd;
    domLocalEventLng;
    domLocalEventLat;
    domBtnEvent;

    btnRefresh;
    localEventList = [];
    map= {};

    start() {
        console.log( 'Application started' );
        this.iniMap();
        this.iniHtml();
        this.grabCoords();
        this.loadLocalEvent();
        this.createMarker();
                
    }

    
    iniMap() {
        
        mapboxgl.accessToken = 'pk.eyJ1IjoiZnJpZ2c2NiIsImEiOiJja25mb21qcWYyaGJ5MnVtcTNydmVsejM1In0.kuXclH7EG88Jv42QRMeCsQ';
        this.map = new mapboxgl.Map({
            container: 'map',
            center: { lng: 115.886, lat: -31.860 },
            zoom: 12.28,
            customAttribution: 'C\'est foutu, on trouvera jamais !',
            style: 'mapbox://styles/frigg66/cknk8czbu1wda17s5ahuww57x'
        });
        
        const zoomCtrl = new mapboxgl.NavigationControl();
        this.map.addControl( zoomCtrl );
        
        const btnRefresh = new RefreshMap;
        this.map.addControl(btnRefresh, 'top-left');
        
    }
    
    iniHtml() {
        this.domLocalEventName = document.querySelector( '#localEventName' );
        this.domLocalEventContent = document.querySelector( '#localEventContent' ); 
        this.domLocalEventStart = document.querySelector( '#localEventStart' );
        this.domLocalEventEnd = document.querySelector( '#localEventEnd' );
        this.domLocalEventLng = document.querySelector( '#eventLng' );
        this.domLocalEventLat = document.querySelector( '#eventLat' );
        this.domBtnEvent = document.querySelector( '#btnEvent' );
        this.domBtnEvent.addEventListener("click", this.createLocalEvent.bind(this));
        this.btnRefresh = document.querySelector('#btnRefresh');
        this.btnRefresh.addEventListener("click", this.refreshMap.bind(this));
    }

    grabCoords() {
         const eventLng = this.domLocalEventLng;
        const eventLat = this.domLocalEventLat;
        
        this.map.on( 'click', function(evt){            
            const coords = evt;            
            eventLng.value = coords.lngLat.lng;
            eventLat.value = coords.lngLat.lat;
        });
    }

    createLocalEvent() {
        const newjsonLocalEvent = {
            name : this.domLocalEventName.value,
            content : this.domLocalEventContent.value,
            eventStart : this.domLocalEventStart.value,
            eventEnd : this.domLocalEventEnd.value,
            eventLng : this.domLocalEventLng.value,
            eventLat : this.domLocalEventLat.value
        }

        const newLocalEvent = new LocalEvent (newjsonLocalEvent);

        this.localEventList.push(newLocalEvent);
        localStorage.setItem(appConfig.localStorageName, JSON.stringify(this.localEventList));
        
        this.domLocalEventName.value = '';
        this.domLocalEventContent.value = '';
        this.domLocalEventStart.value = '';
        this.domLocalEventEnd.value = '';
        this.domLocalEventLng.value = '';
        this.domLocalEventLat.value = '';
        

    }

    loadLocalEvent() {
        const storageContent = localStorage.getItem(appConfig.localStorageName);
        if (storageContent === null ) {
            return;
        }

        let storageContentList;

        try {
            storageContentList = JSON.parse( storageContent );
        } catch(error) {
            localStorage.removeItem(appConfig.localStorageName);
            return;
        }
        for (const storageObject of storageContentList) {
            const newLocalEvent = new LocalEvent (storageObject);
            this.localEventList.push(newLocalEvent);
        }
        console.log(this.localEventList);
    }

    createMarker() {
        
        for ( const localEvent of this.localEventList) {                    
            const marker = new mapboxgl.Marker({
                color: this.setMarkerColor(localEvent.eventStart, localEvent.eventEnd)
            });
            marker.setLngLat( {
                lng: localEvent.eventLng,
                lat: localEvent.eventLat 
            });
            marker.getElement().title = localEvent.name + " du " + localEvent.eventStart + " au " + localEvent.eventEnd;
        
            const popup = new mapboxgl.Popup();
            popup.addClassName('popup');
            popup.setHTML(
                `${localEvent.name} <br>
                ${localEvent.content}<br>
                ${localEvent.eventStart} <br>
                ${localEvent.eventEnd} `
            );
            marker.setPopup( popup );
            marker.addTo( this.map );
        }
    }

    setMarkerColor(eventStart, eventEnd) {
        const presentDay = Date.now();
        const day = 1000 * 60 * 60 * 24;
        let markerColor;
    
        switch(true) {
            case ( ( Date.parse(eventStart) - presentDay ) > (day*3) ):
                markerColor = 'green';
            break;
            case( ( (Date.parse(eventStart) - presentDay ) < (day*3)  && ( Date.parse(eventStart) - presentDay > 0 ) ) ):
                markerColor = 'orange';
            break;
            case ( ( Date.parse(eventStart) < presentDay ) && ( presentDay < Date.parse(eventEnd) ) ) :
                markerColor = 'blue';
            break;
            default:
                markerColor = 'red';
            break;
        }
        return markerColor;        
    }

    refreshMap() {
        this.map = {};
        this.iniMap();
        this.localEventList = [];
        this.loadLocalEvent();
        this.createMarker();
    }
}

const instance = new App();

export default instance;