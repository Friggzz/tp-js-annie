import app from "./App";
export class LocalEvent {

    name;
    content;
    eventStart;
    eventEnd;
    eventLng;
    eventLat;

    constructor(jsonLocalEvent) {
        this.name = jsonLocalEvent.name;
        this.content= jsonLocalEvent.content;
        this.eventStart = jsonLocalEvent.eventStart;
        this.eventEnd = jsonLocalEvent.eventEnd;
        this.eventLng = jsonLocalEvent.eventLng;
        this.eventLat = jsonLocalEvent.eventLat;

    }
}