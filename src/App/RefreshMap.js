import app from "./App";
export class RefreshMap {
    onAdd(map) {
    this.map = map;
    this.container = document.createElement('button');
    this.container.setAttribute('type', 'button');
    this.container.setAttribute('id', 'btnRefresh');
    this.container.className = 'mapboxgl-ctrl';
    this.container.textContent = 'Refresh';
    return this.container;
    }
     
    onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
    }
}